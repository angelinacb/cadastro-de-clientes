package io.github.angelinabonzi.clientesapi.model.entity.repository;

import io.github.angelinabonzi.clientesapi.model.entity.Telefone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClienteTelefoneRepository extends JpaRepository<Telefone, Integer> {

    @Query(" select t from Telefone t join t.cliente c" +
            " where c.id = :id ")
    List<Telefone> findByIdCliente(
            @Param("id") Integer id);

}
