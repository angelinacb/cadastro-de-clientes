package io.github.angelinabonzi.clientesapi.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.validator.constraints.br.CPF;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@Getter
@Setter
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 150)
    @NotEmpty(message = "{campo.nome.obrigatorio}")
    private String nome;

    @Column(length = 1)
    private Integer tipo;

    @Column(length = 11)
    @CPF(message = "{campo.cpf.invalido}")
    private String cpf;

    @Column(length = 14)
    @CNPJ(message = "{campo.cnpj.invalido}")
    private String cnpj;

    @Column(length = 14)
    private String rg;

    @Column(length = 9)
    private String ie;

    @Column(name = "data_cadastro", updatable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataCadastro;

    private Integer ativo;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "id_cliente")
    private List<Telefone> telefones;

    @PrePersist
    public void prePersist(){
        setDataCadastro(LocalDate.now());
    }

}
