package io.github.angelinabonzi.clientesapi.model.entity.api.rest;

import io.github.angelinabonzi.clientesapi.model.entity.Cliente;
import io.github.angelinabonzi.clientesapi.model.entity.Telefone;
import io.github.angelinabonzi.clientesapi.model.entity.repository.ClienteRepository;
import io.github.angelinabonzi.clientesapi.model.entity.repository.TelefoneRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

//Classe que recebe as requisições da API e dá o retorno
@RestController
@RequestMapping("/api/clientes")
@RequiredArgsConstructor
public class ClienteController {

    private final ClienteRepository clienteRepository;
    private final TelefoneRepository telefoneRepository;

    //Salva o cliente
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente save( @RequestBody @Valid Cliente cliente ) {
        return clienteRepository.save(cliente);
    }

    //Salva o telefone
    @PostMapping("{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveTelefone(@PathVariable Integer id,
                                 @RequestBody @Valid Telefone telefone ) {
        clienteRepository
                .findById(id)
                .map(cliente -> {
                    telefone.setCliente(cliente);
                    return telefoneRepository.save(telefone);
                })
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado"));
    }

    //Deleta o cliente
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar ( @PathVariable Integer id ) {
        clienteRepository
                .findById(id)
                .map( cliente -> {
                    clienteRepository.delete(cliente);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado") );
    }

    //Atualiza o cliente
    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizar ( @PathVariable Integer id,
                            @RequestBody @Valid Cliente clienteAtualizado ) {
        clienteRepository
                .findById(id)
                .map( cliente -> {
                    cliente.setNome(clienteAtualizado.getNome());
                    cliente.setTipo(clienteAtualizado.getTipo());
                    cliente.setCpf(clienteAtualizado.getCpf());
                    cliente.setCnpj(clienteAtualizado.getCnpj());
                    cliente.setRg(clienteAtualizado.getRg());
                    cliente.setIe(clienteAtualizado.getIe());
                    cliente.setAtivo(clienteAtualizado.getAtivo());
                    cliente.setTelefones(clienteAtualizado.getTelefones());

                    return clienteRepository.save(cliente);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado") );
    }

    //Retorna uma lista de clientes
    /*@GetMapping
    public List<Cliente> obterTodos() {
        return clienteRepository.findAll();
    }*/

    //Retorna o cliente de um Id passado como parâmetro
    @GetMapping("{id}")
    public Cliente acharPorId( @PathVariable Integer id ){
        return clienteRepository
                .findById(id)
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado") );
    }

    //Pesquisa o cliente (filtros por nome e/ou somente ativos)
    @GetMapping
    public List<Cliente> pesquisar(
            @RequestParam(value = "nome", defaultValue = "") String nome,
            @RequestParam(value = "ativo", defaultValue = "") Integer ativo
    ) {
        return clienteRepository.findByNomeClienteAndAtivo("%" + nome + "%", ativo);
    }
}
