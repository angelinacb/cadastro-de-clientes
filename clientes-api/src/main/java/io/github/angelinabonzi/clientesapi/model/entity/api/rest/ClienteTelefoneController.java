package io.github.angelinabonzi.clientesapi.model.entity.api.rest;

import io.github.angelinabonzi.clientesapi.model.entity.Cliente;
import io.github.angelinabonzi.clientesapi.model.entity.Telefone;
import io.github.angelinabonzi.clientesapi.model.entity.repository.ClienteTelefoneRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

//Classe que recebe as requisições da API e dá o retorno
@RestController
@RequestMapping("/api/telefones")
@RequiredArgsConstructor
public class ClienteTelefoneController {

    private final ClienteTelefoneRepository clienteTelefoneRepository;

    //Retorna uma lista de telefones do cliente passado como parâmetro
    @GetMapping
    public List<Telefone> pesquisar(
            @RequestParam(value = "id", required = false) Integer id
    ) {
        return clienteTelefoneRepository.findByIdCliente(id);
    }

    //Deleta o telefone
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar ( @PathVariable Integer id ) {
        clienteTelefoneRepository
                .findById(id)
                .map( telefone -> {
                    clienteTelefoneRepository.delete(telefone);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Telefone não encontrado") );
    }
}
