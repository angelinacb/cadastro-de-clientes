package io.github.angelinabonzi.clientesapi.model.entity.repository;

import io.github.angelinabonzi.clientesapi.model.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

//Classe responsável por fazer as operações na base de dados para a entidade Cliente
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    @Query(" select c from Cliente c " +
            " where upper( c.nome ) like upper( :nome ) and c.ativo = :ativo " +
               " or upper( c.nome ) like upper( :nome ) " +
               " or c.ativo = :ativo ")
    List<Cliente> findByNomeClienteAndAtivo(
            @Param("nome") String nome, @Param("ativo") Integer ativo);

    //Verifica se já existe cliente com CPF ou CNPJ cadastrado
    @Query(" select c from Cliente c " +
            " where (c.cpf = :cpf or c.cnpj = :cnpj)")
    Optional<Cliente> findByCpfCnpjCliente(
            @Param("cpf") String cpf, @Param("cnpj") String cnpj);


}
