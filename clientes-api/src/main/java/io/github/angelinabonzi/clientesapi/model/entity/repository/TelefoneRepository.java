package io.github.angelinabonzi.clientesapi.model.entity.repository;

import io.github.angelinabonzi.clientesapi.model.entity.Telefone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TelefoneRepository extends JpaRepository<Telefone, Integer> {

}
